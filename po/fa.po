# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Esmail Abbasi <abbasi2244@gmail.com>, 2015
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-03-17 17:04+0000\n"
"PO-Revision-Date: 2015-06-05 08:12+0000\n"
"Last-Translator: Esmail Abbasi <abbasi2244@gmail.com>\n"
"Language-Team: Persian (http://www.transifex.com/mate/MATE/language/fa/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fa\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../lib/mate-menu.py:70
msgid "Menu"
msgstr "منو"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:251
msgid "Couldn't load plugin:"
msgstr "نمیتواند افزونه را بارگیری کند:"

#: ../lib/mate-menu.py:323
msgid "Couldn't initialize plugin"
msgstr "افزونه نمیتواند اجرا شود"

#: ../lib/mate-menu.py:710
msgid "Advanced MATE Menu"
msgstr "منوی حرفه ای MATE"

#: ../lib/mate-menu.py:792
msgid "Preferences"
msgstr "تنظیمات"

#: ../lib/mate-menu.py:795
msgid "Edit menu"
msgstr "ویرایش منو"

#: ../lib/mate-menu.py:798
msgid "Reload plugins"
msgstr "باز خوانی افزونه ها"

#: ../lib/mate-menu.py:801
msgid "About"
msgstr "درباره"

#. i18n
#: ../lib/mate-menu-config.py:50
msgid "Menu preferences"
msgstr "تنظیمات منو"

#: ../lib/mate-menu-config.py:53
msgid "Always start with favorites pane"
msgstr "همیشه هنگام شروع پنجره علاقه مندی ها باز شود"

#: ../lib/mate-menu-config.py:54
msgid "Show button icon"
msgstr "نمایش دکمه آیکن"

#: ../lib/mate-menu-config.py:55
msgid "Use custom colors"
msgstr "انتخاب رنگ های دیگر"

#: ../lib/mate-menu-config.py:56
msgid "Show recent documents plugin"
msgstr "نمایش آخرین سند افزونه"

#: ../lib/mate-menu-config.py:57
msgid "Show applications plugin"
msgstr ""

#: ../lib/mate-menu-config.py:58
msgid "Show system plugin"
msgstr ""

#: ../lib/mate-menu-config.py:59
msgid "Show places plugin"
msgstr ""

#: ../lib/mate-menu-config.py:61
msgid "Show application comments"
msgstr ""

#: ../lib/mate-menu-config.py:62
msgid "Show category icons"
msgstr ""

#: ../lib/mate-menu-config.py:63
msgid "Hover"
msgstr ""

#: ../lib/mate-menu-config.py:64
msgid "Remember the last category or search"
msgstr ""

#: ../lib/mate-menu-config.py:65
msgid "Swap name and generic name"
msgstr ""

#: ../lib/mate-menu-config.py:67
msgid "Border width:"
msgstr ""

#: ../lib/mate-menu-config.py:68
msgid "pixels"
msgstr ""

#: ../lib/mate-menu-config.py:70
msgid "Opacity:"
msgstr ""

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr ""

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr ""

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:255
msgid "Applications"
msgstr ""

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr ""

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:252
#: ../mate_menu/plugins/applications.py:253
msgid "Favorites"
msgstr ""

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr ""

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr ""

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr ""

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr ""

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr ""

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr ""

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr ""

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr ""

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr ""

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr ""

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr ""

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:58
msgid "Places"
msgstr ""

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr ""

#: ../lib/mate-menu-config.py:100
msgid "Show GTK Bookmarks"
msgstr ""

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr ""

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr ""

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:153
msgid "Computer"
msgstr ""

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:164
msgid "Home Folder"
msgstr ""

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:177
msgid "Network"
msgstr ""

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:198
msgid "Desktop"
msgstr ""

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:209
msgid "Trash"
msgstr ""

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr ""

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr ""

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr ""

#: ../lib/mate-menu-config.py:114
#: ../mate_menu/plugins/system_management.py:147
#: ../mate_menu/plugins/system_management.py:150
msgid "Package Manager"
msgstr ""

#: ../lib/mate-menu-config.py:115
#: ../mate_menu/plugins/system_management.py:157
msgid "Control Center"
msgstr ""

#: ../lib/mate-menu-config.py:116
#: ../mate_menu/plugins/system_management.py:164
msgid "Terminal"
msgstr ""

#: ../lib/mate-menu-config.py:117
#: ../mate_menu/plugins/system_management.py:174
msgid "Lock Screen"
msgstr ""

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr ""

#: ../lib/mate-menu-config.py:119
#: ../mate_menu/plugins/system_management.py:192
msgid "Quit"
msgstr ""

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr ""

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr ""

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr ""

#: ../lib/mate-menu-config.py:152
msgid "Keyboard shortcut:"
msgstr ""

#: ../lib/mate-menu-config.py:158
msgid "Images"
msgstr ""

#: ../lib/mate-menu-config.py:267
msgid "Name"
msgstr ""

#: ../lib/mate-menu-config.py:268
msgid "Path"
msgstr ""

#: ../lib/mate-menu-config.py:284
msgid "Desktop theme"
msgstr ""

#: ../lib/mate-menu-config.py:423 ../lib/mate-menu-config.py:454
msgid "Name:"
msgstr ""

#: ../lib/mate-menu-config.py:424 ../lib/mate-menu-config.py:455
msgid "Path:"
msgstr ""

#. i18n
#: ../mate_menu/plugins/applications.py:250
msgid "Search:"
msgstr ""

#: ../mate_menu/plugins/applications.py:254
msgid "All applications"
msgstr ""

#: ../mate_menu/plugins/applications.py:621
#, python-format
msgid "Search Google for %s"
msgstr ""

#: ../mate_menu/plugins/applications.py:628
#, python-format
msgid "Search Wikipedia for %s"
msgstr ""

#: ../mate_menu/plugins/applications.py:644
#, python-format
msgid "Lookup %s in Dictionary"
msgstr ""

#: ../mate_menu/plugins/applications.py:651
#, python-format
msgid "Search Computer for %s"
msgstr ""

#. i18n
#: ../mate_menu/plugins/applications.py:763
#: ../mate_menu/plugins/applications.py:830
msgid "Add to desktop"
msgstr ""

#: ../mate_menu/plugins/applications.py:764
#: ../mate_menu/plugins/applications.py:831
msgid "Add to panel"
msgstr ""

#: ../mate_menu/plugins/applications.py:766
#: ../mate_menu/plugins/applications.py:813
msgid "Insert space"
msgstr ""

#: ../mate_menu/plugins/applications.py:767
#: ../mate_menu/plugins/applications.py:814
msgid "Insert separator"
msgstr ""

#: ../mate_menu/plugins/applications.py:769
#: ../mate_menu/plugins/applications.py:834
msgid "Launch when I log in"
msgstr ""

#: ../mate_menu/plugins/applications.py:771
#: ../mate_menu/plugins/applications.py:836
msgid "Launch"
msgstr ""

#: ../mate_menu/plugins/applications.py:772
msgid "Remove from favorites"
msgstr ""

#: ../mate_menu/plugins/applications.py:774
#: ../mate_menu/plugins/applications.py:839
msgid "Edit properties"
msgstr ""

#. i18n
#: ../mate_menu/plugins/applications.py:812
msgid "Remove"
msgstr ""

#: ../mate_menu/plugins/applications.py:833
msgid "Show in my favorites"
msgstr ""

#: ../mate_menu/plugins/applications.py:837
msgid "Delete from menu"
msgstr ""

#: ../mate_menu/plugins/applications.py:888
msgid "Search Google"
msgstr ""

#: ../mate_menu/plugins/applications.py:895
msgid "Search Wikipedia"
msgstr ""

#: ../mate_menu/plugins/applications.py:905
msgid "Lookup Dictionary"
msgstr ""

#: ../mate_menu/plugins/applications.py:912
msgid "Search Computer"
msgstr ""

#: ../mate_menu/plugins/applications.py:1329
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""

#: ../mate_menu/plugins/applications.py:1529
msgid "All"
msgstr ""

#: ../mate_menu/plugins/applications.py:1529
msgid "Show all applications"
msgstr ""

#: ../mate_menu/plugins/system_management.py:154
msgid "Install, remove and upgrade software packages"
msgstr ""

#: ../mate_menu/plugins/system_management.py:161
msgid "Configure your system"
msgstr ""

#: ../mate_menu/plugins/system_management.py:171
msgid "Use the command line"
msgstr ""

#: ../mate_menu/plugins/system_management.py:182
msgid "Requires password to unlock"
msgstr ""

#: ../mate_menu/plugins/system_management.py:185
msgid "Logout"
msgstr ""

#: ../mate_menu/plugins/system_management.py:189
msgid "Log out or switch user"
msgstr ""

#: ../mate_menu/plugins/system_management.py:196
msgid "Shutdown, restart, suspend or hibernate"
msgstr ""

#: ../mate_menu/plugins/places.py:161
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr ""

#: ../mate_menu/plugins/places.py:172
msgid "Open your personal folder"
msgstr ""

#: ../mate_menu/plugins/places.py:185
msgid "Browse bookmarked and local network locations"
msgstr ""

#: ../mate_menu/plugins/places.py:206
msgid "Browse items placed on the desktop"
msgstr ""

#: ../mate_menu/plugins/places.py:219
msgid "Browse deleted files"
msgstr ""

#: ../mate_menu/plugins/places.py:272
msgid "Empty trash"
msgstr ""

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr ""

#: ../mate_menu/keybinding.py:196
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr ""

#: ../mate_menu/keybinding.py:197
msgid "Press Escape or click again to cancel the operation.  "
msgstr ""

#: ../mate_menu/keybinding.py:198
msgid "Press Backspace to clear the existing keybinding."
msgstr ""

#: ../mate_menu/keybinding.py:211
msgid "Pick an accelerator"
msgstr ""

#: ../mate_menu/keybinding.py:264
msgid "<not set>"
msgstr ""
