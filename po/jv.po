# Javanese translation for MATE Menu
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the package.
# FIRST AUTHOR <www.pilihankedua@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: mate-menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-11 22:31+0000\n"
"PO-Revision-Date: 2013-02-26 09:46+0000\n"
"Last-Translator: Erieck Saputra <erick.1383@gmail.com>\n"
"Language-Team: Javanese <jv@li.org>\n"
"Language: jv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-02-25 10:21+0000\n"
"X-Generator: Launchpad (build 17355)\n"

#: ../lib/mate-menu.py:70
msgid "Menu"
msgstr "Sajian"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:254
msgid "Couldn't load plugin:"
msgstr "Rak iso momot plagin"

#: ../lib/mate-menu.py:326
msgid "Couldn't initialize plugin"
msgstr "Rak iso nginisial plagin"

#: ../lib/mate-menu.py:715
msgid "Advanced MATE Menu"
msgstr ""

#: ../lib/mate-menu.py:798
msgid "Preferences"
msgstr "Acuane"

#: ../lib/mate-menu.py:801
msgid "Edit menu"
msgstr "Ngubah sajian"

#: ../lib/mate-menu.py:804
msgid "Reload plugins"
msgstr "Ngulangi plagin"

#: ../lib/mate-menu.py:807
msgid "About"
msgstr "Sekitaran"

#. i18n
#: ../lib/mate-menu-config.py:50
msgid "Menu preferences"
msgstr "Acuan'e sajian"

#: ../lib/mate-menu-config.py:53
msgid "Always start with favorites pane"
msgstr "mesti'o ng'awali karô jendelô senengan'e"

#: ../lib/mate-menu-config.py:54
msgid "Show button icon"
msgstr "Tampil'no tombôl ikon"

#: ../lib/mate-menu-config.py:55
msgid "Use custom colors"
msgstr "Ng'go werno kulinanê"

#: ../lib/mate-menu-config.py:56
msgid "Show recent documents plugin"
msgstr "nampilnô naskah plagin anyar"

#: ../lib/mate-menu-config.py:57
msgid "Show applications plugin"
msgstr "tampilnô terap'an plagin"

#: ../lib/mate-menu-config.py:58
msgid "Show system plugin"
msgstr "tampilnô sistim plagin"

#: ../lib/mate-menu-config.py:59
msgid "Show places plugin"
msgstr "tampilnô ng'gonan plagin"

#: ../lib/mate-menu-config.py:61
msgid "Show application comments"
msgstr "Nampilno komentarane aplikasi"

#: ../lib/mate-menu-config.py:62
msgid "Show category icons"
msgstr "Nampilno kelompok'e ikon"

#: ../lib/mate-menu-config.py:63
msgid "Hover"
msgstr "M'blayang"

#: ../lib/mate-menu-config.py:64
msgid "Remember the last category or search"
msgstr ""

#: ../lib/mate-menu-config.py:65
msgid "Swap name and generic name"
msgstr "Nami swap lan nami jenerik"

#: ../lib/mate-menu-config.py:67
msgid "Border width:"
msgstr "Lebar pinggiran'e:"

#: ../lib/mate-menu-config.py:68
msgid "pixels"
msgstr "Piksel'e"

#: ../lib/mate-menu-config.py:70
msgid "Opacity:"
msgstr "burêm'e"

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr "Tombôl teks"

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "Kekarep'an"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:252
msgid "Applications"
msgstr "Terapan'ne"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr "Tema"

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:249
#: ../mate_menu/plugins/applications.py:250
msgid "Favorites"
msgstr "Senengan'e"

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr "Tombol utami"

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr ""

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "Layar-dasar:"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr "Pos judul:"

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr "Pinggirane:"

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr "Têma:"

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr "Nomer seko lajurane:"

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "Ukurane ikon:"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr "Nundane mblayang (ms):"

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "Tombol ikon:"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr "NgGoleki perintahe:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:58
msgid "Places"
msgstr "Panggonan"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr "Ngolehno bar-ngGulir"

#: ../lib/mate-menu-config.py:100
msgid "Show GTK+ Bookmarks"
msgstr "Nampilno GTK+ Pratondo-buku"

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "Dhuwure:"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr "Kotakan Panggonan Semulone:"

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:153
msgid "Computer"
msgstr "Kompiuter"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:164
msgid "Home Folder"
msgstr "Map Omah"

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:177
msgid "Network"
msgstr "Jejaringkerjo"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:198
msgid "Desktop"
msgstr "Tetampilan"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:209
msgid "Trash"
msgstr "Ekrak"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr "Panggonan Kulinone:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "Sistim"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr "Ceklekan Bekakas Gawanane:"

#: ../lib/mate-menu-config.py:114
#: ../mate_menu/plugins/system_management.py:149
#: ../mate_menu/plugins/system_management.py:152
#: ../mate_menu/plugins/system_management.py:155
msgid "Package Manager"
msgstr "Manajer Paket"

#: ../lib/mate-menu-config.py:115
#: ../mate_menu/plugins/system_management.py:162
msgid "Control Center"
msgstr "Pusate Ngontroli"

#: ../lib/mate-menu-config.py:116
#: ../mate_menu/plugins/system_management.py:169
msgid "Terminal"
msgstr "Treminal"

#: ../lib/mate-menu-config.py:117
#: ../mate_menu/plugins/system_management.py:179
msgid "Lock Screen"
msgstr "Ngunci Sekat"

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "Log Metu"

#: ../lib/mate-menu-config.py:119
#: ../mate_menu/plugins/system_management.py:197
msgid "Quit"
msgstr "Mbedal"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr "Ngrubah Panggonan"

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr "Panggonan Anyar"

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr "Mileh sakwiji map"

#: ../lib/mate-menu-config.py:152
msgid "Keyboard shortcut:"
msgstr "Kibot pintas:"

#: ../lib/mate-menu-config.py:158
msgid "Images"
msgstr "gambar"

#: ../lib/mate-menu-config.py:267
msgid "Name"
msgstr "Nami"

#: ../lib/mate-menu-config.py:268
msgid "Path"
msgstr "Rintis"

#: ../lib/mate-menu-config.py:284
msgid "Desktop theme"
msgstr "empôk'e tetampil"

#: ../lib/mate-menu-config.py:432 ../lib/mate-menu-config.py:463
msgid "Name:"
msgstr "Nami:"

#: ../lib/mate-menu-config.py:433 ../lib/mate-menu-config.py:464
msgid "Path:"
msgstr "Pet:"

#. i18n
#: ../mate_menu/plugins/applications.py:247
msgid "Search:"
msgstr "madôsi"

#: ../mate_menu/plugins/applications.py:251
msgid "All applications"
msgstr "Kabeh aplikasine"

#: ../mate_menu/plugins/applications.py:618
#, python-format
msgid "Search Google for %s"
msgstr "madôsi google 'nggo %s"

#: ../mate_menu/plugins/applications.py:625
#, python-format
msgid "Search Wikipedia for %s"
msgstr "madôsi Wikipedia 'nggo %s"

#: ../mate_menu/plugins/applications.py:641
#, python-format
msgid "Lookup %s in Dictionary"
msgstr "ndelok'en %s ing kamus"

#: ../mate_menu/plugins/applications.py:648
#, python-format
msgid "Search Computer for %s"
msgstr "madôsi kompiuter 'nggo %s"

#. i18n
#: ../mate_menu/plugins/applications.py:761
#: ../mate_menu/plugins/applications.py:830
msgid "Add to desktop"
msgstr "nambah rêng tetampil"

#: ../mate_menu/plugins/applications.py:762
#: ../mate_menu/plugins/applications.py:831
msgid "Add to panel"
msgstr "nambah rêng krentil'an"

#: ../mate_menu/plugins/applications.py:764
#: ../mate_menu/plugins/applications.py:812
msgid "Insert space"
msgstr "ngLebokno ruangan"

#: ../mate_menu/plugins/applications.py:765
#: ../mate_menu/plugins/applications.py:813
msgid "Insert separator"
msgstr "ngLebokno sekatan"

#: ../mate_menu/plugins/applications.py:767
#: ../mate_menu/plugins/applications.py:834
msgid "Launch when I log in"
msgstr "ngLuncurake ketiko aku log mlebu"

#: ../mate_menu/plugins/applications.py:769
#: ../mate_menu/plugins/applications.py:836
msgid "Launch"
msgstr "Ngluncurake"

#: ../mate_menu/plugins/applications.py:770
msgid "Remove from favorites"
msgstr "ngGuwak seko favorite"

#: ../mate_menu/plugins/applications.py:772
#: ../mate_menu/plugins/applications.py:839
msgid "Edit properties"
msgstr "'ngrubah sifat'e"

#. i18n
#: ../mate_menu/plugins/applications.py:811
msgid "Remove"
msgstr "ngGuwak"

#: ../mate_menu/plugins/applications.py:833
msgid "Show in my favorites"
msgstr "Tampilno ing kesenenganku"

#: ../mate_menu/plugins/applications.py:837
msgid "Delete from menu"
msgstr "mbusak sêko sajian"

#: ../mate_menu/plugins/applications.py:888
msgid "Search Google"
msgstr "madôsi google"

#: ../mate_menu/plugins/applications.py:895
msgid "Search Wikipedia"
msgstr "madôsi Wikipedia"

#: ../mate_menu/plugins/applications.py:905
msgid "Lookup Dictionary"
msgstr "ndelok kamus"

#: ../mate_menu/plugins/applications.py:912
msgid "Search Computer"
msgstr "madôsi kompiuter"

#: ../mate_menu/plugins/applications.py:1332
#, fuzzy
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""
"Rakiso nyimpen favorite. Ngecekko tek kowe nduwe akses reng ~/.linuxmint/"
"mintMenu"

#: ../mate_menu/plugins/applications.py:1532
msgid "All"
msgstr "Sedoyo"

#: ../mate_menu/plugins/applications.py:1532
msgid "Show all applications"
msgstr "Nampilno sedoyo aplikasi-aplikasi"

#: ../mate_menu/plugins/system_management.py:159
msgid "Install, remove and upgrade software packages"
msgstr "Nginstal, ngGuwak, lan ngeapgred paket prangkat lunak"

#: ../mate_menu/plugins/system_management.py:166
msgid "Configure your system"
msgstr "Susunane sistem sampeyan"

#: ../mate_menu/plugins/system_management.py:176
msgid "Use the command line"
msgstr "ngGanggo garis printah"

#: ../mate_menu/plugins/system_management.py:187
msgid "Requires password to unlock"
msgstr "Mbutuhno sandi kanggo mbukake"

#: ../mate_menu/plugins/system_management.py:190
msgid "Logout"
msgstr "Log metu"

#: ../mate_menu/plugins/system_management.py:194
msgid "Log out or switch user"
msgstr "Log metu utowo skakel pengguno"

#: ../mate_menu/plugins/system_management.py:201
msgid "Shutdown, restart, suspend or hibernate"
msgstr "Ngademke, mbaleni nguripno, nundo, tah turu"

#: ../mate_menu/plugins/places.py:161
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr ""
"Nelusuri kabeh gludegkan lan pengendali disk lan map akses seko komputer iki"

#: ../mate_menu/plugins/places.py:172
msgid "Open your personal folder"
msgstr "Mbukak map pribadimu"

#: ../mate_menu/plugins/places.py:185
msgid "Browse bookmarked and local network locations"
msgstr "Nelusuri pratondo-buku lan sakgludegkan lokasine jaringan"

#: ../mate_menu/plugins/places.py:206
msgid "Browse items placed on the desktop"
msgstr "Nelusuri panggonane perkakas nok destop"

#: ../mate_menu/plugins/places.py:219
msgid "Browse deleted files"
msgstr "Nelusuri brekas sing keguwak"

#: ../mate_menu/plugins/places.py:272
msgid "Empty trash"
msgstr "Ngosongi ekrak"

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr "Arsip saat iki"

#: ../mate_menu/keybinding.py:199
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr ""

#: ../mate_menu/keybinding.py:200
msgid "Press Escape or click again to cancel the operation.  "
msgstr ""

#: ../mate_menu/keybinding.py:201
msgid "Press Backspace to clear the existing keybinding."
msgstr ""

#: ../mate_menu/keybinding.py:214
msgid "Pick an accelerator"
msgstr ""

#: ../mate_menu/keybinding.py:267
msgid "<not set>"
msgstr ""

#~ msgid "Search for packages to install"
#~ msgstr "nggoleki paket kanggo install"

#~ msgid "Software Manager"
#~ msgstr "Manajer Prangkat lunak"

#~ msgid "Install package '%s'"
#~ msgstr "ngInstall paket '%s'"

#~ msgid "Uninstall"
#~ msgstr "Mreteli"

#~ msgid "Find Software"
#~ msgstr "'golek'i bekakas-empuk"

#~ msgid "Find Tutorials"
#~ msgstr "'golek'i warah'an"

#~ msgid "Find Hardware"
#~ msgstr "'golek'i bekakas-atôs"

#~ msgid "Find Ideas"
#~ msgstr "'golek'i 'gagasan"

#~ msgid "Find Users"
#~ msgstr "'golek'i pengguno"

#~ msgid "Browse and install available software"
#~ msgstr "Nelusuri lan masang cemepake prangkat lunak"

#~ msgid "Show sidepane"
#~ msgstr "Tampelno panel samping"

#~ msgid "Colors"
#~ msgstr "Werno"

#~ msgid "File not found"
#~ msgstr "Brekas rak ditemukno"

#~ msgid "Show recent documents"
#~ msgstr "Tampilno dokument terakhire"

#~ msgid "Based on USP from S.Chanderbally"
#~ msgstr "Dedasarê nok USP sêko S.Chanderbally"

#~ msgid "Advanced Gnome Menu"
#~ msgstr "Sajian gnome nglanjut"

#~ msgid "Please wait, this can take some time"
#~ msgstr "Monggo nunggu, iki iso mangan wektu"

#~ msgid "Application removed successfully"
#~ msgstr "Guwake aplikasi wis kasil"

#~ msgid "No matching package found"
#~ msgstr "Rak'ono pakêt cocok sing ditemuk'no"

#~ msgid "Do you want to remove this menu entry?"
#~ msgstr "Opo kowe pingin nôk guwak mlebunê sajian iki ?"

#~ msgid "Packages to be removed"
#~ msgstr "Pepaket'an sing diguwak"

#~ msgid "The following packages will be removed:"
#~ msgstr "Ngetutno paketan sing pingin diguwak:"

#~ msgid "Not a .menu file"
#~ msgstr "Orak sakwijine .menu brekas"

#~ msgid "Not a valid .menu file"
#~ msgstr "Orak sing sah .menu brekas"

#~ msgid "Filter:"
#~ msgstr "Saringan:"

#~ msgid "Search portal for '%s'"
#~ msgstr "Nelusuri portal kanggo '%s'"

#~ msgid "Search repositories for '%s'"
#~ msgstr "Nelusuri repositori kanggo '%s'"

#~ msgid "Show package '%s'"
#~ msgstr "Tampilno paket '%s'"

#~ msgid "Remove %s?"
#~ msgstr "ngGuwak %s?"
