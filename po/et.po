# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Ivar Smolin <okul@linux.ee>, 2015
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-03-17 17:04+0000\n"
"PO-Revision-Date: 2015-11-10 22:17+0000\n"
"Last-Translator: Ivar Smolin <okul@linux.ee>\n"
"Language-Team: Estonian (http://www.transifex.com/mate/MATE/language/et/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: et\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../lib/mate-menu.py:70
msgid "Menu"
msgstr "Menüü"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:251
msgid "Couldn't load plugin:"
msgstr "Pluginat pole võimalik laadida:"

#: ../lib/mate-menu.py:323
msgid "Couldn't initialize plugin"
msgstr "Pluginat pole võimalk lähtestada"

#: ../lib/mate-menu.py:710
msgid "Advanced MATE Menu"
msgstr ""

#: ../lib/mate-menu.py:792
msgid "Preferences"
msgstr "Eelistused"

#: ../lib/mate-menu.py:795
msgid "Edit menu"
msgstr "Redigeeri menüüd"

#: ../lib/mate-menu.py:798
msgid "Reload plugins"
msgstr ""

#: ../lib/mate-menu.py:801
msgid "About"
msgstr "Programmist lähemalt"

#. i18n
#: ../lib/mate-menu-config.py:50
msgid "Menu preferences"
msgstr "Menüü eelistused"

#: ../lib/mate-menu-config.py:53
msgid "Always start with favorites pane"
msgstr ""

#: ../lib/mate-menu-config.py:54
msgid "Show button icon"
msgstr "Näita nupu ikooni"

#: ../lib/mate-menu-config.py:55
msgid "Use custom colors"
msgstr "Kastuta kohandatud värve"

#: ../lib/mate-menu-config.py:56
msgid "Show recent documents plugin"
msgstr "Näisa viimati kasutatud dokumentide pluginat"

#: ../lib/mate-menu-config.py:57
msgid "Show applications plugin"
msgstr "Näita rakenduste pluginat"

#: ../lib/mate-menu-config.py:58
msgid "Show system plugin"
msgstr "Näita süsteemi pluginat"

#: ../lib/mate-menu-config.py:59
msgid "Show places plugin"
msgstr "Näita asukohtade pluginat"

#: ../lib/mate-menu-config.py:61
msgid "Show application comments"
msgstr "Näita rakenduse kommentaare"

#: ../lib/mate-menu-config.py:62
msgid "Show category icons"
msgstr "Näita kategooriate ikoone"

#: ../lib/mate-menu-config.py:63
msgid "Hover"
msgstr ""

#: ../lib/mate-menu-config.py:64
msgid "Remember the last category or search"
msgstr "Mäleta viimast kategooriat või otsingut"

#: ../lib/mate-menu-config.py:65
msgid "Swap name and generic name"
msgstr ""

#: ../lib/mate-menu-config.py:67
msgid "Border width:"
msgstr "Äärise laius"

#: ../lib/mate-menu-config.py:68
msgid "pixels"
msgstr "pikslit"

#: ../lib/mate-menu-config.py:70
msgid "Opacity:"
msgstr "Läbipaistmatus:"

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr "Nupu tekst:"

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "Valikud"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:255
msgid "Applications"
msgstr "Rakendused"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr "Teema"

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:252
#: ../mate_menu/plugins/applications.py:253
msgid "Favorites"
msgstr "Lemmikud"

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr "Põhinupp"

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr "Pluginad"

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "Taust:"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr "Pealkirjad:"

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr "Äärised:"

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr "Teema:"

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr "Veergude arv:"

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "Ikooni suurus:"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr ""

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "Nupu ikoon:"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr "Otsimise käsk:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:58
msgid "Places"
msgstr "Asukohad"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr "Luba kerimisriba"

#: ../lib/mate-menu-config.py:100
msgid "Show GTK Bookmarks"
msgstr "Näita GTK järjehoidjaid"

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "Kõrgus:"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr ""

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:153
msgid "Computer"
msgstr "Arvuti"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:164
msgid "Home Folder"
msgstr "Kodukataloog"

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:177
msgid "Network"
msgstr "Võrk"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:198
msgid "Desktop"
msgstr "Töölaud"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:209
msgid "Trash"
msgstr "Prügi"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr "Kohandatud asukohad:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "Süsteem"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr ""

#: ../lib/mate-menu-config.py:114
#: ../mate_menu/plugins/system_management.py:147
#: ../mate_menu/plugins/system_management.py:150
msgid "Package Manager"
msgstr "Pakihaldur"

#: ../lib/mate-menu-config.py:115
#: ../mate_menu/plugins/system_management.py:157
msgid "Control Center"
msgstr "Juhtimiskeskus"

#: ../lib/mate-menu-config.py:116
#: ../mate_menu/plugins/system_management.py:164
msgid "Terminal"
msgstr "Terminal"

#: ../lib/mate-menu-config.py:117
#: ../mate_menu/plugins/system_management.py:174
msgid "Lock Screen"
msgstr "Lukusta ekraan"

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "Logi välja"

#: ../lib/mate-menu-config.py:119
#: ../mate_menu/plugins/system_management.py:192
msgid "Quit"
msgstr "Lõpeta"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr "Muuda asukohta"

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr "Uus asukoht"

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr "Vali kataloog"

#: ../lib/mate-menu-config.py:152
msgid "Keyboard shortcut:"
msgstr "Kiirklahv:"

#: ../lib/mate-menu-config.py:158
msgid "Images"
msgstr "Pildid"

#: ../lib/mate-menu-config.py:267
msgid "Name"
msgstr "Nimi"

#: ../lib/mate-menu-config.py:268
msgid "Path"
msgstr "Rada"

#: ../lib/mate-menu-config.py:284
msgid "Desktop theme"
msgstr "Töölaua teema"

#: ../lib/mate-menu-config.py:423 ../lib/mate-menu-config.py:454
msgid "Name:"
msgstr "Nimi:"

#: ../lib/mate-menu-config.py:424 ../lib/mate-menu-config.py:455
msgid "Path:"
msgstr "Rada:"

#. i18n
#: ../mate_menu/plugins/applications.py:250
msgid "Search:"
msgstr "Otsing:"

#: ../mate_menu/plugins/applications.py:254
msgid "All applications"
msgstr "Kõik rakendused"

#: ../mate_menu/plugins/applications.py:621
#, python-format
msgid "Search Google for %s"
msgstr ""

#: ../mate_menu/plugins/applications.py:628
#, python-format
msgid "Search Wikipedia for %s"
msgstr ""

#: ../mate_menu/plugins/applications.py:644
#, python-format
msgid "Lookup %s in Dictionary"
msgstr ""

#: ../mate_menu/plugins/applications.py:651
#, python-format
msgid "Search Computer for %s"
msgstr ""

#. i18n
#: ../mate_menu/plugins/applications.py:763
#: ../mate_menu/plugins/applications.py:830
msgid "Add to desktop"
msgstr "Lisa töölauale"

#: ../mate_menu/plugins/applications.py:764
#: ../mate_menu/plugins/applications.py:831
msgid "Add to panel"
msgstr "Lisa paneelile"

#: ../mate_menu/plugins/applications.py:766
#: ../mate_menu/plugins/applications.py:813
msgid "Insert space"
msgstr ""

#: ../mate_menu/plugins/applications.py:767
#: ../mate_menu/plugins/applications.py:814
msgid "Insert separator"
msgstr "Lisa eraldaja"

#: ../mate_menu/plugins/applications.py:769
#: ../mate_menu/plugins/applications.py:834
msgid "Launch when I log in"
msgstr ""

#: ../mate_menu/plugins/applications.py:771
#: ../mate_menu/plugins/applications.py:836
msgid "Launch"
msgstr ""

#: ../mate_menu/plugins/applications.py:772
msgid "Remove from favorites"
msgstr "Eemalda lemmikutest"

#: ../mate_menu/plugins/applications.py:774
#: ../mate_menu/plugins/applications.py:839
msgid "Edit properties"
msgstr "Redigeeri omadusi"

#. i18n
#: ../mate_menu/plugins/applications.py:812
msgid "Remove"
msgstr "Eemalda"

#: ../mate_menu/plugins/applications.py:833
msgid "Show in my favorites"
msgstr "Näita minu lemmikutes"

#: ../mate_menu/plugins/applications.py:837
msgid "Delete from menu"
msgstr "Kustuta menüüst"

#: ../mate_menu/plugins/applications.py:888
msgid "Search Google"
msgstr "Otsi Googlest"

#: ../mate_menu/plugins/applications.py:895
msgid "Search Wikipedia"
msgstr "Otsi Vikipeedaist"

#: ../mate_menu/plugins/applications.py:905
msgid "Lookup Dictionary"
msgstr "Otsi sõnastikust"

#: ../mate_menu/plugins/applications.py:912
msgid "Search Computer"
msgstr "Otsi arvutit"

#: ../mate_menu/plugins/applications.py:1329
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""

#: ../mate_menu/plugins/applications.py:1529
msgid "All"
msgstr "Kõik"

#: ../mate_menu/plugins/applications.py:1529
msgid "Show all applications"
msgstr "Näita kõiki rakendusi"

#: ../mate_menu/plugins/system_management.py:154
msgid "Install, remove and upgrade software packages"
msgstr "Tarkvarapakkide paigaldus, eemaldus ja uuendus"

#: ../mate_menu/plugins/system_management.py:161
msgid "Configure your system"
msgstr "Seadista süsteemi"

#: ../mate_menu/plugins/system_management.py:171
msgid "Use the command line"
msgstr "Kasuta käsurida"

#: ../mate_menu/plugins/system_management.py:182
msgid "Requires password to unlock"
msgstr ""

#: ../mate_menu/plugins/system_management.py:185
msgid "Logout"
msgstr "Logi välja"

#: ../mate_menu/plugins/system_management.py:189
msgid "Log out or switch user"
msgstr "Logi välja või vaheta kasutajat"

#: ../mate_menu/plugins/system_management.py:196
msgid "Shutdown, restart, suspend or hibernate"
msgstr ""

#: ../mate_menu/plugins/places.py:161
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr "Kõigi kohalike ja võrgus asuvate ketaste sirvimine"

#: ../mate_menu/plugins/places.py:172
msgid "Open your personal folder"
msgstr "Sinu isikliku kataloogi avamine"

#: ../mate_menu/plugins/places.py:185
msgid "Browse bookmarked and local network locations"
msgstr "Järjehoidjates või kohalikus võrgus asuvate asukohtade sirvimine"

#: ../mate_menu/plugins/places.py:206
msgid "Browse items placed on the desktop"
msgstr "Töölaual olevate kirjete sirvimine"

#: ../mate_menu/plugins/places.py:219
msgid "Browse deleted files"
msgstr "Kustutatud failide sirvimine"

#: ../mate_menu/plugins/places.py:272
msgid "Empty trash"
msgstr "Prügikasti tühjendamine"

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr "Viimati kasutatud dokumendid"

#: ../mate_menu/keybinding.py:196
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr "Klõpsa menüü avamiseks ja sulgemiseks mõeldud uue kiirklahvi määramiseks"

#: ../mate_menu/keybinding.py:197
msgid "Press Escape or click again to cancel the operation.  "
msgstr "Tegevuse tühistamiseks vajuta Escape või klõpsa uuesti"

#: ../mate_menu/keybinding.py:198
msgid "Press Backspace to clear the existing keybinding."
msgstr "Olemasoleva kiirklahvi tühistamiseks vajuta Backspace klahvi."

#: ../mate_menu/keybinding.py:211
msgid "Pick an accelerator"
msgstr ""

#: ../mate_menu/keybinding.py:264
msgid "<not set>"
msgstr "<määramata>"
