# Hindi translation for MATE Menu
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: mate-menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-11 22:31+0000\n"
"PO-Revision-Date: 2011-09-20 14:12+0000\n"
"Last-Translator: Ashutosh Kumar Singh <Unknown>\n"
"Language-Team: Hindi <hi@li.org>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-02-25 10:21+0000\n"
"X-Generator: Launchpad (build 17355)\n"

#: ../lib/mate-menu.py:70
msgid "Menu"
msgstr "मेनू"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:254
msgid "Couldn't load plugin:"
msgstr "प्लगिन लोड नहीं हो सका:"

#: ../lib/mate-menu.py:326
msgid "Couldn't initialize plugin"
msgstr "प्लगिन इनिशियलाइज़ नहीं हो सका"

#: ../lib/mate-menu.py:715
msgid "Advanced MATE Menu"
msgstr ""

#: ../lib/mate-menu.py:798
msgid "Preferences"
msgstr "वरीयताएँ"

#: ../lib/mate-menu.py:801
msgid "Edit menu"
msgstr "मेन्यू संपादित करें"

#: ../lib/mate-menu.py:804
msgid "Reload plugins"
msgstr "प्लगइन पुनः लोड करें"

#: ../lib/mate-menu.py:807
msgid "About"
msgstr "विवरण"

#. i18n
#: ../lib/mate-menu-config.py:50
msgid "Menu preferences"
msgstr "मेनू वरीयताएँ"

#: ../lib/mate-menu-config.py:53
msgid "Always start with favorites pane"
msgstr "हमेशा पसंदीदा पटल के साथ शुरू करें"

#: ../lib/mate-menu-config.py:54
msgid "Show button icon"
msgstr "बटन चिह्न दिखाएँ"

#: ../lib/mate-menu-config.py:55
msgid "Use custom colors"
msgstr "कस्टम रंग का प्रयोग करें"

#: ../lib/mate-menu-config.py:56
msgid "Show recent documents plugin"
msgstr ""

#: ../lib/mate-menu-config.py:57
msgid "Show applications plugin"
msgstr ""

#: ../lib/mate-menu-config.py:58
msgid "Show system plugin"
msgstr ""

#: ../lib/mate-menu-config.py:59
msgid "Show places plugin"
msgstr ""

#: ../lib/mate-menu-config.py:61
msgid "Show application comments"
msgstr "दिखाएँ ऐप्प्लिकेशन टिप्पणियाँ"

#: ../lib/mate-menu-config.py:62
msgid "Show category icons"
msgstr "श्रेणी आइकनों दिखाएँ"

#: ../lib/mate-menu-config.py:63
msgid "Hover"
msgstr "मंडराएं"

#: ../lib/mate-menu-config.py:64
msgid "Remember the last category or search"
msgstr ""

#: ../lib/mate-menu-config.py:65
msgid "Swap name and generic name"
msgstr "नाम और जेनेरिक नाम को स्वैप करें"

#: ../lib/mate-menu-config.py:67
msgid "Border width:"
msgstr "सीमा चौड़ाई:"

#: ../lib/mate-menu-config.py:68
msgid "pixels"
msgstr "पिक्सेल"

#: ../lib/mate-menu-config.py:70
msgid "Opacity:"
msgstr "अपारदर्शिताः"

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr "बटन टेक्स्ट:"

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "विकल्प"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:252
msgid "Applications"
msgstr "ऐप्प्लिकेशन"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr ""

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:249
#: ../mate_menu/plugins/applications.py:250
msgid "Favorites"
msgstr "पसंदीदा"

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr "मुख्य बटन"

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr ""

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "पृष्ठभूमि:"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr "शीर्षकों:"

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr "सीमाएं:"

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr ""

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr "कॉलम की संख्या:"

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "आइकन माप:"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr "मंडरान देरी (एमएस):"

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "बटन आइकन:"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr "कमांड खोजें:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:58
msgid "Places"
msgstr "स्थान"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr "स्क्रोलबार को अनुमति दें"

#: ../lib/mate-menu-config.py:100
msgid "Show GTK+ Bookmarks"
msgstr ""

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "ऊँचाई:"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr "डिफ़ॉल्ट स्थान टॉगल करें:"

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:153
msgid "Computer"
msgstr "कंप्यूटर"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:164
msgid "Home Folder"
msgstr "गृह फ़ोल्डर"

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:177
msgid "Network"
msgstr "नेटवर्क"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:198
msgid "Desktop"
msgstr "डेस्कटॉप"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:209
msgid "Trash"
msgstr "कचरा"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr "कस्टम स्थान:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "सिस्टम"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr "डिफ़ॉल्ट आइटम टॉगल करें:"

#: ../lib/mate-menu-config.py:114
#: ../mate_menu/plugins/system_management.py:149
#: ../mate_menu/plugins/system_management.py:152
#: ../mate_menu/plugins/system_management.py:155
msgid "Package Manager"
msgstr "पैकेज प्रबंधक"

#: ../lib/mate-menu-config.py:115
#: ../mate_menu/plugins/system_management.py:162
msgid "Control Center"
msgstr "नियंत्रण केंद्र"

#: ../lib/mate-menu-config.py:116
#: ../mate_menu/plugins/system_management.py:169
msgid "Terminal"
msgstr "टर्मिनल"

#: ../lib/mate-menu-config.py:117
#: ../mate_menu/plugins/system_management.py:179
msgid "Lock Screen"
msgstr "स्क्रीन पर ताला लगाएं"

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "लॉग आउट"

#: ../lib/mate-menu-config.py:119
#: ../mate_menu/plugins/system_management.py:197
msgid "Quit"
msgstr "छोडें"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr "स्थान संपादित करें"

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr "नया स्थान"

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr "फ़ोल्डर का चयन करें"

#: ../lib/mate-menu-config.py:152
msgid "Keyboard shortcut:"
msgstr "कुंजीपटल शॉर्टकट:"

#: ../lib/mate-menu-config.py:158
msgid "Images"
msgstr ""

#: ../lib/mate-menu-config.py:267
msgid "Name"
msgstr "नाम"

#: ../lib/mate-menu-config.py:268
msgid "Path"
msgstr "पथ"

#: ../lib/mate-menu-config.py:284
msgid "Desktop theme"
msgstr ""

#: ../lib/mate-menu-config.py:432 ../lib/mate-menu-config.py:463
msgid "Name:"
msgstr "नाम:"

#: ../lib/mate-menu-config.py:433 ../lib/mate-menu-config.py:464
msgid "Path:"
msgstr "पथ:"

#. i18n
#: ../mate_menu/plugins/applications.py:247
msgid "Search:"
msgstr ""

#: ../mate_menu/plugins/applications.py:251
msgid "All applications"
msgstr "सभी ऐप्प्लिकेशन"

#: ../mate_menu/plugins/applications.py:618
#, python-format
msgid "Search Google for %s"
msgstr ""

#: ../mate_menu/plugins/applications.py:625
#, python-format
msgid "Search Wikipedia for %s"
msgstr ""

#: ../mate_menu/plugins/applications.py:641
#, python-format
msgid "Lookup %s in Dictionary"
msgstr ""

#: ../mate_menu/plugins/applications.py:648
#, python-format
msgid "Search Computer for %s"
msgstr ""

#. i18n
#: ../mate_menu/plugins/applications.py:761
#: ../mate_menu/plugins/applications.py:830
msgid "Add to desktop"
msgstr "डेस्कटॉप पर जोड़ें"

#: ../mate_menu/plugins/applications.py:762
#: ../mate_menu/plugins/applications.py:831
msgid "Add to panel"
msgstr "पटल पर जोड़ें"

#: ../mate_menu/plugins/applications.py:764
#: ../mate_menu/plugins/applications.py:812
msgid "Insert space"
msgstr "जगह डालें"

#: ../mate_menu/plugins/applications.py:765
#: ../mate_menu/plugins/applications.py:813
msgid "Insert separator"
msgstr "विभाजक डालें"

#: ../mate_menu/plugins/applications.py:767
#: ../mate_menu/plugins/applications.py:834
msgid "Launch when I log in"
msgstr "जब मैं लॉग इन करूँ तब चलाएँ"

#: ../mate_menu/plugins/applications.py:769
#: ../mate_menu/plugins/applications.py:836
msgid "Launch"
msgstr "चलाएँ"

#: ../mate_menu/plugins/applications.py:770
msgid "Remove from favorites"
msgstr "पसंदीदा से निकालें"

#: ../mate_menu/plugins/applications.py:772
#: ../mate_menu/plugins/applications.py:839
msgid "Edit properties"
msgstr "प्रोपर्टीज़ को सम्पादित करें"

#. i18n
#: ../mate_menu/plugins/applications.py:811
msgid "Remove"
msgstr "हटाएँ"

#: ../mate_menu/plugins/applications.py:833
msgid "Show in my favorites"
msgstr "मेरे पसंदीदा में दिखाएँ"

#: ../mate_menu/plugins/applications.py:837
msgid "Delete from menu"
msgstr "मेनू से हटायें"

#: ../mate_menu/plugins/applications.py:888
msgid "Search Google"
msgstr ""

#: ../mate_menu/plugins/applications.py:895
msgid "Search Wikipedia"
msgstr ""

#: ../mate_menu/plugins/applications.py:905
msgid "Lookup Dictionary"
msgstr ""

#: ../mate_menu/plugins/applications.py:912
msgid "Search Computer"
msgstr ""

#: ../mate_menu/plugins/applications.py:1332
#, fuzzy
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""
"पसंदीदा सुरक्षित नहीं कर सके . जाँच करें कि आपके पास ~/.linuxmint/mintMenu में  लिखने की "
"अनुमति है"

#: ../mate_menu/plugins/applications.py:1532
msgid "All"
msgstr "समस्त"

#: ../mate_menu/plugins/applications.py:1532
msgid "Show all applications"
msgstr "सभी ऐप्प्लिकेशन दिखाएँ"

#: ../mate_menu/plugins/system_management.py:159
msgid "Install, remove and upgrade software packages"
msgstr "स्थापित करें, हटाएँ और सॉफ्टवेयर पैकेज उन्नत करें"

#: ../mate_menu/plugins/system_management.py:166
msgid "Configure your system"
msgstr "अपने सिस्टम को कॉन्फ़िगर करें"

#: ../mate_menu/plugins/system_management.py:176
msgid "Use the command line"
msgstr "आदेश पंक्ति का उपयोग करें"

#: ../mate_menu/plugins/system_management.py:187
msgid "Requires password to unlock"
msgstr "अनलॉक करने के लिए पासवर्ड की आवश्यकता है"

#: ../mate_menu/plugins/system_management.py:190
msgid "Logout"
msgstr "लॉगआउट"

#: ../mate_menu/plugins/system_management.py:194
msgid "Log out or switch user"
msgstr "लॉग आउट या उपयोगकर्ता स्विच करें"

#: ../mate_menu/plugins/system_management.py:201
msgid "Shutdown, restart, suspend or hibernate"
msgstr "बंद करें, पुनः आरंभ करें, निलंबित करें या सीतनिद्रा में डालें"

#: ../mate_menu/plugins/places.py:161
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr "ब्राउज़ करें सभी स्थानीय और दूरदराज के डिस्क और फ़ोल्डर्स इस कंप्यूटर के पहुंच में"

#: ../mate_menu/plugins/places.py:172
msgid "Open your personal folder"
msgstr "अपना व्यक्तिगत फ़ोल्डर खोलें"

#: ../mate_menu/plugins/places.py:185
msgid "Browse bookmarked and local network locations"
msgstr "बुकमार्क और स्थानीय नेटवर्क स्थानों ब्राउज़ करें"

#: ../mate_menu/plugins/places.py:206
msgid "Browse items placed on the desktop"
msgstr "ब्राउज़ करें डेस्कटॉप पर रखे सामान"

#: ../mate_menu/plugins/places.py:219
msgid "Browse deleted files"
msgstr "ब्राउज़ करें नष्ट कर दी गयीं फ़ाइलें"

#: ../mate_menu/plugins/places.py:272
msgid "Empty trash"
msgstr "कचरा खाली करें"

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr "हाल के दस्तावेजों"

#: ../mate_menu/keybinding.py:199
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr ""

#: ../mate_menu/keybinding.py:200
msgid "Press Escape or click again to cancel the operation.  "
msgstr ""

#: ../mate_menu/keybinding.py:201
msgid "Press Backspace to clear the existing keybinding."
msgstr ""

#: ../mate_menu/keybinding.py:214
msgid "Pick an accelerator"
msgstr ""

#: ../mate_menu/keybinding.py:267
msgid "<not set>"
msgstr ""

#~ msgid "Software Manager"
#~ msgstr "सॉफ्टवेयर प्रबंधक"

#~ msgid "Install package '%s'"
#~ msgstr "स्थापित करें पैकेज '%s'"

#~ msgid "Uninstall"
#~ msgstr "स्थापना रद्द करें"

#~ msgid "Browse and install available software"
#~ msgstr "ब्राउज़ करें और उपलब्ध सॉफ्टवेयर स्थापित करें"

#~ msgid "Colors"
#~ msgstr "रंग"

#~ msgid "Filter:"
#~ msgstr "फ़िल्टर:"

#~ msgid "Please wait, this can take some time"
#~ msgstr "कृपया प्रतीक्षा करें, यह कुछ समय ले सकता है"

#~ msgid "Advanced Gnome Menu"
#~ msgstr "उन्नत ग्नोम मेन्यू"

#~ msgid "Based on USP from S.Chanderbally"
#~ msgstr "एस.चन्दर्बल्ली के युएसपी पर आधारित"

#~ msgid "Application removed successfully"
#~ msgstr "ऐप्प्लिकेशन सफलतापूर्वक हटा दिया"

#~ msgid "No matching package found"
#~ msgstr "कोई समान पैकेज नहीं मिला"

#~ msgid "Do you want to remove this menu entry?"
#~ msgstr "क्या आप इस मेन्यू प्रविष्टि को हटाना चाहते हैं?"

#~ msgid "Remove %s?"
#~ msgstr "हटाएँ %s?"

#~ msgid "The following packages will be removed:"
#~ msgstr "निम्नलिखित पैकेज हटा दिए जाएँगे:"

#~ msgid "Packages to be removed"
#~ msgstr "पैकेज जिन्हें हटाना है"

#~ msgid "Show sidepane"
#~ msgstr "साइडपेन दिखाएँ"

#~ msgid "Show recent documents"
#~ msgstr "दिखाएँ हाल ही में इस्तमाल दस्तावेज"

#~ msgid "File not found"
#~ msgstr "फ़ाइल नहीं मिला"

#~ msgid "Not a .menu file"
#~ msgstr "यह .menu फ़ाइल नहीं है"

#~ msgid "Not a valid .menu file"
#~ msgstr "यह वैध .menu फ़ाइल नहीं है"

#~ msgid "Search portal for '%s'"
#~ msgstr "पोर्टल में खोजें '%s'"

#~ msgid "Search repositories for '%s'"
#~ msgstr "रेपोज़िट्री में खोजें '%s'"

#~ msgid "Show package '%s'"
#~ msgstr "दिखाएँ पैकेज '%s'"
