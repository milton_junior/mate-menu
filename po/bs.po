# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Sky Lion <xskylionx@gmail.com>, 2016-2017
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-03-17 17:04+0000\n"
"PO-Revision-Date: 2017-01-15 15:16+0000\n"
"Last-Translator: Sky Lion <xskylionx@gmail.com>\n"
"Language-Team: Bosnian (http://www.transifex.com/mate/MATE/language/bs/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: bs\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../lib/mate-menu.py:70
msgid "Menu"
msgstr "Meni"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:251
msgid "Couldn't load plugin:"
msgstr "Nije moglo učitati plugin:"

#: ../lib/mate-menu.py:323
msgid "Couldn't initialize plugin"
msgstr ""

#: ../lib/mate-menu.py:710
msgid "Advanced MATE Menu"
msgstr "Napredni MATE Meni"

#: ../lib/mate-menu.py:792
msgid "Preferences"
msgstr "Postavke"

#: ../lib/mate-menu.py:795
msgid "Edit menu"
msgstr "Uredi meni"

#: ../lib/mate-menu.py:798
msgid "Reload plugins"
msgstr "Ponovno učitaj plugine"

#: ../lib/mate-menu.py:801
msgid "About"
msgstr "O"

#. i18n
#: ../lib/mate-menu-config.py:50
msgid "Menu preferences"
msgstr "Postavke menija"

#: ../lib/mate-menu-config.py:53
msgid "Always start with favorites pane"
msgstr ""

#: ../lib/mate-menu-config.py:54
msgid "Show button icon"
msgstr ""

#: ../lib/mate-menu-config.py:55
msgid "Use custom colors"
msgstr "Koristi vlastite boje"

#: ../lib/mate-menu-config.py:56
msgid "Show recent documents plugin"
msgstr ""

#: ../lib/mate-menu-config.py:57
msgid "Show applications plugin"
msgstr ""

#: ../lib/mate-menu-config.py:58
msgid "Show system plugin"
msgstr ""

#: ../lib/mate-menu-config.py:59
msgid "Show places plugin"
msgstr ""

#: ../lib/mate-menu-config.py:61
msgid "Show application comments"
msgstr "Prikaži komentare aplikacija"

#: ../lib/mate-menu-config.py:62
msgid "Show category icons"
msgstr "Prikaži ikone kategorija"

#: ../lib/mate-menu-config.py:63
msgid "Hover"
msgstr ""

#: ../lib/mate-menu-config.py:64
msgid "Remember the last category or search"
msgstr "Zapamti zadnju kategoriju ili pretragu"

#: ../lib/mate-menu-config.py:65
msgid "Swap name and generic name"
msgstr ""

#: ../lib/mate-menu-config.py:67
msgid "Border width:"
msgstr "Širina granice:"

#: ../lib/mate-menu-config.py:68
msgid "pixels"
msgstr "pikseli"

#: ../lib/mate-menu-config.py:70
msgid "Opacity:"
msgstr ""

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr "Tekst dugma:"

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "Opcije"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:255
msgid "Applications"
msgstr "Aplikacije"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr "Tema"

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:252
#: ../mate_menu/plugins/applications.py:253
msgid "Favorites"
msgstr "Omiljeni"

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr "Glavno dugme"

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr "Plugini"

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "Pozadina:"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr ""

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr "Granice:"

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr "Tema:"

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr "Broj kolona:"

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "Veličina ikone:"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr ""

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "Ikona dugma:"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr ""

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:58
msgid "Places"
msgstr "Mjesta"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr ""

#: ../lib/mate-menu-config.py:100
msgid "Show GTK Bookmarks"
msgstr "Prikaži GTK Zabilješke"

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "Visina:"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr ""

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:153
msgid "Computer"
msgstr "Računar"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:164
msgid "Home Folder"
msgstr "Početni direktorij"

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:177
msgid "Network"
msgstr "Mreža"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:198
msgid "Desktop"
msgstr "Radna površina"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:209
msgid "Trash"
msgstr "Smeće"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr "Postavljena mjesta:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "Sistem"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr ""

#: ../lib/mate-menu-config.py:114
#: ../mate_menu/plugins/system_management.py:147
#: ../mate_menu/plugins/system_management.py:150
msgid "Package Manager"
msgstr "Paketni Menadžer"

#: ../lib/mate-menu-config.py:115
#: ../mate_menu/plugins/system_management.py:157
msgid "Control Center"
msgstr "Kontrolni Centar"

#: ../lib/mate-menu-config.py:116
#: ../mate_menu/plugins/system_management.py:164
msgid "Terminal"
msgstr "Terminal"

#: ../lib/mate-menu-config.py:117
#: ../mate_menu/plugins/system_management.py:174
msgid "Lock Screen"
msgstr ""

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "Odjava"

#: ../lib/mate-menu-config.py:119
#: ../mate_menu/plugins/system_management.py:192
msgid "Quit"
msgstr "Izađi"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr "Uredi mjesto"

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr "Novo mjesto"

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr "Odaberi folder"

#: ../lib/mate-menu-config.py:152
msgid "Keyboard shortcut:"
msgstr "Skraćenica:"

#: ../lib/mate-menu-config.py:158
msgid "Images"
msgstr "Slike"

#: ../lib/mate-menu-config.py:267
msgid "Name"
msgstr "Ime"

#: ../lib/mate-menu-config.py:268
msgid "Path"
msgstr "Putanja"

#: ../lib/mate-menu-config.py:284
msgid "Desktop theme"
msgstr "Tema Radne Površine"

#: ../lib/mate-menu-config.py:423 ../lib/mate-menu-config.py:454
msgid "Name:"
msgstr "Ime:"

#: ../lib/mate-menu-config.py:424 ../lib/mate-menu-config.py:455
msgid "Path:"
msgstr "Putanja:"

#. i18n
#: ../mate_menu/plugins/applications.py:250
msgid "Search:"
msgstr "Traži:"

#: ../mate_menu/plugins/applications.py:254
msgid "All applications"
msgstr "Sve aplikacije"

#: ../mate_menu/plugins/applications.py:621
#, python-format
msgid "Search Google for %s"
msgstr "Pretraži Google za %s"

#: ../mate_menu/plugins/applications.py:628
#, python-format
msgid "Search Wikipedia for %s"
msgstr "Pretraži Wikipediju za %s"

#: ../mate_menu/plugins/applications.py:644
#, python-format
msgid "Lookup %s in Dictionary"
msgstr "Traži za %s u riječniku"

#: ../mate_menu/plugins/applications.py:651
#, python-format
msgid "Search Computer for %s"
msgstr "Pretraži Računar za %s"

#. i18n
#: ../mate_menu/plugins/applications.py:763
#: ../mate_menu/plugins/applications.py:830
msgid "Add to desktop"
msgstr "Dodaj na radnu površinu"

#: ../mate_menu/plugins/applications.py:764
#: ../mate_menu/plugins/applications.py:831
msgid "Add to panel"
msgstr "Dodaj na panel"

#: ../mate_menu/plugins/applications.py:766
#: ../mate_menu/plugins/applications.py:813
msgid "Insert space"
msgstr ""

#: ../mate_menu/plugins/applications.py:767
#: ../mate_menu/plugins/applications.py:814
msgid "Insert separator"
msgstr "Unesi razmak"

#: ../mate_menu/plugins/applications.py:769
#: ../mate_menu/plugins/applications.py:834
msgid "Launch when I log in"
msgstr "Pokreni kad se prijavim"

#: ../mate_menu/plugins/applications.py:771
#: ../mate_menu/plugins/applications.py:836
msgid "Launch"
msgstr "Pokreni"

#: ../mate_menu/plugins/applications.py:772
msgid "Remove from favorites"
msgstr "Ukloni sa omiljeni"

#: ../mate_menu/plugins/applications.py:774
#: ../mate_menu/plugins/applications.py:839
msgid "Edit properties"
msgstr "Uredi postavke"

#. i18n
#: ../mate_menu/plugins/applications.py:812
msgid "Remove"
msgstr "Ukloni"

#: ../mate_menu/plugins/applications.py:833
msgid "Show in my favorites"
msgstr "Prikaži u mojim omiljenim"

#: ../mate_menu/plugins/applications.py:837
msgid "Delete from menu"
msgstr "Obriši sa menija"

#: ../mate_menu/plugins/applications.py:888
msgid "Search Google"
msgstr "Pretraži Google"

#: ../mate_menu/plugins/applications.py:895
msgid "Search Wikipedia"
msgstr "Pretraži Wikipediju"

#: ../mate_menu/plugins/applications.py:905
msgid "Lookup Dictionary"
msgstr ""

#: ../mate_menu/plugins/applications.py:912
msgid "Search Computer"
msgstr "Pretraži Računar"

#: ../mate_menu/plugins/applications.py:1329
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""

#: ../mate_menu/plugins/applications.py:1529
msgid "All"
msgstr "Sve"

#: ../mate_menu/plugins/applications.py:1529
msgid "Show all applications"
msgstr "Prikaži sve aplikacije"

#: ../mate_menu/plugins/system_management.py:154
msgid "Install, remove and upgrade software packages"
msgstr "Instalirajte, uklonite i ažurirajte pakete"

#: ../mate_menu/plugins/system_management.py:161
msgid "Configure your system"
msgstr "Podesite vaš sistem"

#: ../mate_menu/plugins/system_management.py:171
msgid "Use the command line"
msgstr "Koristi komandnu liniju"

#: ../mate_menu/plugins/system_management.py:182
msgid "Requires password to unlock"
msgstr "Potrebna je šifra za otključavanje"

#: ../mate_menu/plugins/system_management.py:185
msgid "Logout"
msgstr "Odjavi se"

#: ../mate_menu/plugins/system_management.py:189
msgid "Log out or switch user"
msgstr "Odjavite se ili promijenite korisnika"

#: ../mate_menu/plugins/system_management.py:196
msgid "Shutdown, restart, suspend or hibernate"
msgstr "Ugasite, ponovno pokrenite, suspendirajte ili hibernirajte računar"

#: ../mate_menu/plugins/places.py:161
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr ""

#: ../mate_menu/plugins/places.py:172
msgid "Open your personal folder"
msgstr "Otvori lični folder"

#: ../mate_menu/plugins/places.py:185
msgid "Browse bookmarked and local network locations"
msgstr "Pregledajte zabilježene i lokalne mrežne lokacije"

#: ../mate_menu/plugins/places.py:206
msgid "Browse items placed on the desktop"
msgstr "Pregledajte stavke postavljene na radnoj površini"

#: ../mate_menu/plugins/places.py:219
msgid "Browse deleted files"
msgstr "Pregledajte obrisane fajlove"

#: ../mate_menu/plugins/places.py:272
msgid "Empty trash"
msgstr "Isprazni smeće"

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr "Skorašnji dokumenti"

#: ../mate_menu/keybinding.py:196
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr ""

#: ../mate_menu/keybinding.py:197
msgid "Press Escape or click again to cancel the operation.  "
msgstr ""

#: ../mate_menu/keybinding.py:198
msgid "Press Backspace to clear the existing keybinding."
msgstr ""

#: ../mate_menu/keybinding.py:211
msgid "Pick an accelerator"
msgstr ""

#: ../mate_menu/keybinding.py:264
msgid "<not set>"
msgstr ""
